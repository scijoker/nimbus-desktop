package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import rx.Observable;
import sample.net.NimbusService;
import sample.net.requestbody.SignInRequestBody;
import sample.net.responsebody.SignInResponseBody;

public class MainAppController {
    private MainApp mainApp;
    private Stage appStage;
    private NimbusService nimbusService;

    @FXML
    private TextField tFieldLogin;

    @FXML
    private TextField tFieldPassword;

    @FXML
    private void initialize() {
    }

    public void setNimbusService(NimbusService nimbusService) {
        this.nimbusService = nimbusService;
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setAppStage(Stage appStage) {
        this.appStage = appStage;
    }

    @FXML
    private void handleSignInButtonClick() {
        String login = tFieldLogin.getText();
        String password = tFieldPassword.getText();

        Observable<SignInResponseBody> signInResponseBodyObservable = nimbusService.signIn(new SignInRequestBody(login, password));
        signInResponseBodyObservable.subscribe(signInResponseBody -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Authorization was successful!");
            alert.setContentText("token: " + signInResponseBody.body.token);
            alert.show();
        });

    }
}
