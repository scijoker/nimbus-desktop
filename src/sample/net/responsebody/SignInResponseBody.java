package sample.net.responsebody;

/**
 * Created by scijoker on 03.01.16.
 */
public class SignInResponseBody {
    public int errorCode;
    public Body body;

    public class Body {
        public String sessionid;
        public String token;
        public Info info;

        public class Info {
            public String user_id;
            public String register_date;
            public int days_of_quota_reset;
            public Premium premium;
            public Usage usage;
            public Limits limits;

            public class Premium {
                public boolean active;
                public String start_date;
                public String end_date;
                public String source;


            }

            public class Usage {
                public Notes notes;

                public class Notes {
                    public long current;
                    public long max;
                }
            }

            public class Limits {
                public long NOTES_MAX_SIZE;
                public long NOTES_MONTH_USAGE_QUOTA;
                public long NOTES_MAX_ATTACHMENT_SIZE;
            }
        }
    }
}
