package sample.net.requestbody;

/**
 * Created by scijoker on 03.01.16.
 */
public class SignInRequestBody {
    public String action = "user:auth";
    public String _client_software = "android_notes";
    public Body body;

    public class Body {
        public String email;
        public String password;

        public Body(String email, String password) {
            this.email = email;
            this.password = password;
        }
    }

    public SignInRequestBody(String email, String password) {
        this.body = new Body(email, password);
    }
}
