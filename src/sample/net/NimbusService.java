package sample.net;

import retrofit.http.Body;
import retrofit.http.POST;
import rx.Observable;
import sample.net.requestbody.SignInRequestBody;
import sample.net.responsebody.SignInResponseBody;

/**
 * Created by scijoker on 03.01.16.
 */
public interface NimbusService {
    @POST("/")
    Observable<SignInResponseBody> signIn(@Body SignInRequestBody body);
}
