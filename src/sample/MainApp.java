package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import sample.net.NimbusService;

public class MainApp extends Application {
    private GridPane appLayout;
    private MainAppController appController;
    private Stage primaryStage;
    private static NimbusService nimbusService;

    @Override
    public void start(Stage primaryStage) throws Exception {
        initRetrofit();

        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Nimbus Note request test");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("main.fxml"));
        appLayout = loader.load();
        initAppController(loader);
        primaryStage.setScene(new Scene(appLayout));
        primaryStage.show();

    }

    private void initRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl("https://sync.everhelper.me")
                .build();
        nimbusService = retrofit.create(NimbusService.class);
    }

    public static NimbusService getNimbusService() {
        return nimbusService;
    }

    private void initAppController(FXMLLoader loader) {
        appController = loader.getController();
        appController.setMainApp(this);
        appController.setAppStage(getPrimaryStage());
        appController.setNimbusService(getNimbusService());
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
